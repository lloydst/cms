Rails.application.routes.draw do
    root 'blogs#index'
    mount RailsAdmin::Engine => '/dashboard', as: 'rails_admin'
    devise_for :users
    
    resources :blogs
    # to use custom devise controllers do: 
    # Rails.application.routes.draw do
    #   devise_for :users, controllers: {
    #     sessions: 'users/sessions'
    #   }
    # end
    # but none are customised so let keep it commented out for now
end
