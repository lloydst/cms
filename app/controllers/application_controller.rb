class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
# load_and_authorize_resource
    before_action :authenticate_user!
    def after_sign_in_path_for(resource)
        rails_admin_path
    end
    # catch not enough authorisation
    rescue_from CanCan::AccessDenied do | exception |
        redirect_to root_url, alert: exception.message
    end
end
